DROP DATABASE IF EXISTS inventorydb;

CREATE DATABASE inventorydb
	ENCODING = 'UTF8'
	LC_COLLATE = 'en_US.UTF-8'
	LC_CTYPE = 'en_US.UTF-8'
	TABLESPACE = pg_default
	OWNER = inventory
	TEMPLATE  = template0;

\c inventorydb inventory		

DROP SCHEMA IF EXISTS inventory CASCADE;
CREATE SCHEMA inventory;

CREATE TABLE inventory.permission(
	permission_id character varying(50),
	persmission character varying(50),
	CONSTRAINT pk_permision PRIMARY KEY (permission_id)
);

CREATE TABLE inventory.group(
	group_id character varying(50),
	group_name character varying(64),
	CONSTRAINT pk_group PRIMARY KEY (group_id)
);

CREATE TABLE inventory.group_permission(
	group_id character varying(50),
	permission_id character varying(50),
	CONSTRAINT pk_group_permision PRIMARY KEY (group_id,permission_id),
	FOREIGN KEY (group_id) REFERENCES inventory.group(group_id),
	FOREIGN KEY (permission_id) REFERENCES inventory.permission(permission_id)
);

CREATE TABLE inventory.user(
	user_id character varying(50),
	first_name character varying(50) NOT NULL,
	last_name character varying(50),
	email character varying(50) NOT NULL,
	contact_no character varying(50) NOT NULL,
	alt_contact_no character varying(50),
	street character varying(50),
	city character varying(50),
	state character varying(50),
	is_deleted BOOLEAN DEFAULT FALSE,
	is_active BOOLEAN DEFAULT FALSE,
	pincode character varying(50),
	origin character varying(50),
	create_date character varying(50) NOT NULL,
	created_by character varying(50),
	update_date character varying(50),
	update_by character varying(50),
	CONSTRAINT PK_inventoty_user PRIMARY KEY (user_id)

);

CREATE TABLE inventory.user_group(
	user_id character varying(50),
	group_id character varying(50),
	origin character varying(50),
	CONSTRAINT PK_USER_GROUP_ID PRIMARY KEY (user_id,group_id),
	FOREIGN KEY (user_id) REFERENCES inventory.user(user_id),
	FOREIGN KEY (group_id) REFERENCES inventory.group(group_id)
);

CREATE TABLE inventory.brand(
	brand_id character varying(50),
	brand_name character varying(50),
	description character varying(60),
	origin character varying(50),
	create_date character varying(30),
	created_by character varying(50),
	updated_date character varying(50),
	updated_by character varying(50),
	CONSTRAINT PK_brand PRIMARY KEY (brand_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
);

CREATE TABLE inventory.category (
	category_id character varying(50),
	category_name character varying(50),
	description character varying(50),
	origin character varying(50),
	create_date character varying(30),
	created_by character varying(50),
	updated_date character varying(50),
	updated_by character varying(50),
	PRIMARY KEY (category_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
);

CREATE TABLE inventory.sub_category (
	sub_category_id character varying(50),
	sub_category_name character varying(50),
	category_id character varying(50),
	description character varying(60),
	origin character varying(50),
	create_date character varying(30),
	created_by character varying(50),
	updated_date character varying(50),
	updated_by character varying(50),
	CONSTRAINT pk_subcategory PRIMARY KEY (sub_category_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (category_id) REFERENCES inventory.category(category_id)
);


CREATE TABLE inventory.product (
	product_id character varying(50),
	product_code character varying(56),
	product_name character varying(120),
	category_id character varying(50),
	sub_category_id character varying(50),
	manufacturing_date character varying(50),
	expiry_date character varying(50),
	brand_id character varying(50),
	description character varying(640),
	base_price numeric(10,2),
	sellig_price numeric(10,2),
	qr_code character varying(400),
	bar_code character varying(400),
	quantity integer,
	origin character varying(50),
	is_active boolean DEFAULT TRUE,
	create_date character varying(30),
	created_by character varying(50),
	updated_date character varying(50),
	updated_by character varying(50),
	PRIMARY KEY (product_id),
	FOREIGN KEY (category_id) REFERENCES inventory.category(category_id),
	FOREIGN KEY (sub_category_id) REFERENCES inventory.sub_category(sub_category_id),
	FOREIGN KEY (brand_id) REFERENCES inventory.brand(brand_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
);

CREATE TABLE inventory.order(
	order_id character varying(50),
	user_id character varying(50),
	order_status character varying(50),
	purchase_mode character varying(50),
	payment_mode character varying(50),
	origin character varying(50),
	create_date character varying(50),
	update_date character varying(50),
	created_by character varying(50),
	updated_by character varying(50),
	PRIMARY KEY (order_id),
	FOREIGN KEY (user_id) REFERENCES inventory.user(user_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
);

CREATE TABLE inventory.order_items(
	order_item_id character varying(50),
	order_id character varying(50),
	product_id character varying(50),
	quantity integer,
	price numeric(10,2),
	discount integer,
	origin character varying(50),
	create_date character varying(50),
	update_date character varying(50),
	created_by character varying(50),
	updated_by character varying(50),
	CONSTRAINT PK_ORDER_ITEMS PRIMARY KEY (order_item_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
);

CREATE TABLE inventory.notification_template(
	notification_id character varying(50),
	notification_type character varying(50),
	email_sender character varying(160),
	email_body TEXT,
	email_subject character varying(160),
	sms_body character varying(160),
	sms_sender character varying(12),
	push_body character varying(500),
	origin character varying(50),
	--	add fields for push notification
	create_date  character varying(50),
	update_date  character varying(50),
	created_by  character varying(50),
	updated_by  character varying(50),
	PRIMARY KEY (notification_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
	
);

CREATE TABLE inventory.supplier_type(
	supplier_type_id character varying(50),
	supplier_type_name character varying(50),
	description character varying(120),
	origin character varying(50),
	create_date  character varying(50),
	update_date  character varying(50),
	created_by  character varying(50),
	updated_by  character varying(50),
	CONSTRAINT pk_supplier_type_id PRIMARY KEY (supplier_type_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
);

CREATE TABLE inventory.supplier(
	supplier_id character varying(50),
	supplier_name character varying(50),
	supplier_type_id character varying(50),
	description character varying(120),
	is_deleted character varying(50),
	is_active character varying(50),
	street character varying(50),
	city character varying(50),
	state character varying(50), 
	pincode character varying(50),
	contact_no character varying(50),
	alt_contact_no character varying(50),
	contact_person character varying(50),
	alt_contact_person character varying(50),
	origin character varying(50),
	create_date character varying(50),
	updated_date character varying(50),
	created_by character varying(50),
	updated_by character varying(50),
	CONSTRAINT pk_supplier_id PRIMARY KEY (supplier_id),
	FOREIGN KEY (supplier_type_id) REFERENCES inventory.supplier_type(supplier_type_id),
	FOREIGN KEY (created_by) REFERENCES inventory.user(user_id),
	FOREIGN KEY (updated_by) REFERENCES inventory.user(user_id)
); 

CREATE TABLE inventory.product_supplier(
	product_supplier_id SERIAL,
	product_id character varying(50),
	supplier_id character varying(50),
	origin character varying(50),
	FOREIGN KEY (product_id) REFERENCES inventory.product(product_id),
	FOREIGN KEY (supplier_id) REFERENCES inventory.supplier(supplier_id)
);


